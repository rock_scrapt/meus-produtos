package cadastroproduto.leandro.souza.meusprodutos.repository.local;

import android.arch.persistence.room.Room;

import cadastroproduto.leandro.souza.meusprodutos.infrastructure.Constants;
//import cadastroproduto.leandro.souza.meusprodutos.infrastructure.MyApplication;

public final class DatabaseManager {

    private MyDatabase myDatabase;
    private static DatabaseManager instance;

    private DatabaseManager() {
        myDatabase = Room.databaseBuilder(MyApplication.getInstance().getApplicationContext(), MyDatabase.class,
                Constants.Database.DATABASE_NAME)
                .fallbackToDestructiveMigration()
                .build();
    }

    public synchronized static DatabaseManager getInstance() {
        if (instance == null) {
            instance = new DatabaseManager();
        }

        return instance;
    }

    public MyDatabase getMyDatabase() {
        return myDatabase;
    }
}