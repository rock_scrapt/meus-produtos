package cadastroproduto.leandro.souza.meusprodutos.repository.remote;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import cadastroproduto.leandro.souza.meusprodutos.infrastructure.Constants;

public final class ApiManager {

    private Retrofit retrofit;
    private static ApiManager instance;

    private ApiManager() {
        retrofit = new Retrofit.Builder()
                .baseUrl(Constants.ApiService.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }

    public static ApiManager getInstance() {
        if (instance == null) {
            instance = new ApiManager();
        }

        return instance;
    }

    public <T> T create(Class<T> service) {
        return retrofit.create(service);
    }
}