package cadastroproduto.leandro.souza.meusprodutos.repository.model;

import com.google.gson.annotations.SerializedName;

public class Credentials {

    @SerializedName("usuario")
    private String username;
    @SerializedName("senha")
    private String password;

    public Credentials() { }

    public Credentials(String username, String password) {
        this.username = username;
        this.password = password;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}