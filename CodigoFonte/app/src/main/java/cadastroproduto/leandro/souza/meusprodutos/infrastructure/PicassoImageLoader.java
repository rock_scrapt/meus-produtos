package cadastroproduto.leandro.souza.meusprodutos.infrastructure;

import android.widget.ImageView;

import com.squareup.picasso.Picasso;

/**
 * Created by patricio on 26/11/17.
 */

public class PicassoImageLoader implements ImageLoader {

    @Override
    public void loadImage(ImageView view, String source) {
        Picasso.with(view.getContext()).load(source).into(view);
    }

    @Override
    public void loadImage(ImageView view, String source, int size) {
        Picasso.with(view.getContext())
                .load(source)
                .resize(size, size)
                .centerCrop()
                .into(view);
    }
}