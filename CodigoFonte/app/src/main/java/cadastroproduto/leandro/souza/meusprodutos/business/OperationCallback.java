package cadastroproduto.leandro.souza.meusprodutos.business;

public interface OperationCallback<T> {

    void onCallback(T data, Throwable failure);
}
