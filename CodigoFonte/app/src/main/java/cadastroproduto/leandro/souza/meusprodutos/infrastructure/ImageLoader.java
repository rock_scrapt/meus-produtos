package cadastroproduto.leandro.souza.meusprodutos.infrastructure;

import android.widget.ImageView;

/**
 * Created by patricio on 26/11/17.
 */

public interface ImageLoader {
    void loadImage(ImageView view, String source);
    void loadImage(ImageView view, String source, int size);
}