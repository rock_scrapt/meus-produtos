package cadastroproduto.leandro.souza.meusprodutos.presentation.login;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.CheckBox;
import android.widget.TextView;
import android.widget.Toast;

import cadastroproduto.leandro.souza.meusprodutos.R;

public class LoginActivity extends AppCompatActivity {

    TextView txtLogin, txtSenha, chMantem;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        txtLogin = (TextView) findViewById(R.id.editUser);
        txtSenha = (TextView) findViewById(R.id.editSenha);
        chMantem = (CheckBox) findViewById(R.id.chManter);


    }

    public void login(View view){


        if(chMantem.isClickable()){
            Toast.makeText(this, "Manter conectado", Toast.LENGTH_LONG).show();
        }
        else{
            Toast.makeText(this, "Ação do botao", Toast.LENGTH_LONG).show();
        }
    }
}
