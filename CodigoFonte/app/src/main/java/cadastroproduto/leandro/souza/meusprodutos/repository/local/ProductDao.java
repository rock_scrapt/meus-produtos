package cadastroproduto.leandro.souza.meusprodutos.repository.local;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

import cadastroproduto.leandro.souza.meusprodutos.repository.model.Product;

@Dao
public interface ProductDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertProducts(List<Product> products);

    @Delete
    int deleteProduct(Product product);

    @Update
    int updateProduct(Product product);

    @Query("SELECT * FROM product")
    List<Product> getAllProducts();
}