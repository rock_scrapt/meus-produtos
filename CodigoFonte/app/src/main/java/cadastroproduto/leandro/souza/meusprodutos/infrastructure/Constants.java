package cadastroproduto.leandro.souza.meusprodutos.infrastructure;

public final class Constants {

    public static final class ApiService {

        public static final String BASE_URL = "http://apiteste.fourtime.com/api/";
    }

    public static final class Database {

        public static final String DATABASE_NAME = "cadastro_produto";
    }

    public static final class Preferences {

        public static final String USER = "user";
    }

    public static final class Params {

        public static final String PRODUCT = "product";
        public static final String USER = "user";
        public static final String DIALOG_WHICH = "dialog_which";
        public static final String DIALOG_MESSAGE = "dialog_message";
        public static final String DIALOG_POSITIVE_BUTTTON = "dialog_positive_buttton";
        public static final String DIALOG_NEGATIVE_BUTTTON = "dialog_negative_buttton";
    }

    private Constants() {}
}