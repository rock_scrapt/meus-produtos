package cadastroproduto.leandro.souza.meusprodutos.infrastructure;

import android.content.res.Resources;

public final class DimenUtil {

    public static int dpToPx(int dp) {
        return (int) (dp * Resources.getSystem().getDisplayMetrics().density);
    }
}