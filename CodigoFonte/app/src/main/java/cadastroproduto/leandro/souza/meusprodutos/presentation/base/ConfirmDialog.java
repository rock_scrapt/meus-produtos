package cadastroproduto.leandro.souza.meusprodutos.presentation.base;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;

import cadastroproduto.leandro.souza.meusprodutos.infrastructure.Constants;

public class ConfirmDialog extends DialogFragment {

    public enum Choice { POSITIVE, NEGATIVE }

    public interface ConfirmDialogListener {
        void onConfirmChoice(int which, Choice choice);
    }

    private ConfirmDialogListener listener;

    public static ConfirmDialog newInstance(int which, String message,
                                            String positiveButton, String negativeButton) {
        Bundle params = new Bundle();
        params.putInt(Constants.Params.DIALOG_WHICH, which);
        params.putString(Constants.Params.DIALOG_MESSAGE, message);
        params.putString(Constants.Params.DIALOG_POSITIVE_BUTTTON, positiveButton);
        params.putString(Constants.Params.DIALOG_NEGATIVE_BUTTTON, negativeButton);

        ConfirmDialog dialog = new ConfirmDialog();
        dialog.setArguments(params);

        return dialog;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        if (context instanceof ConfirmDialogListener) {
            listener = (ConfirmDialogListener) context;
            return;
        }

        Fragment fragment = getParentFragment();

        if (fragment != null && fragment instanceof  ConfirmDialogListener) {
            listener = (ConfirmDialogListener) fragment;
        }
    }

    @Override
    public void onDetach() {
        listener = null;
        super.onDetach();
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final int dialogWhich = getArguments().getInt(Constants.Params.DIALOG_WHICH);

        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setMessage(getArguments().getString(Constants.Params.DIALOG_MESSAGE));

        builder.setPositiveButton(getArguments().getString(Constants.Params.DIALOG_POSITIVE_BUTTTON),
                new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int which) {
                onChoice(dialogWhich, Choice.POSITIVE);
            }
        });

        builder.setNegativeButton(getArguments().getString(Constants.Params.DIALOG_NEGATIVE_BUTTTON),
                new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int which) {
                onChoice(dialogWhich, Choice.NEGATIVE);
            }
        });

        return builder.create();
    }

    private void onChoice(int which, Choice choice) {
        if (listener != null) {
            listener.onConfirmChoice(which, choice);
        }
    }
}