package cadastroproduto.leandro.souza.meusprodutos.repository.remote;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ApiResponse<T> {

    @SerializedName("lista")
    private List<T> items;
    @SerializedName("sucesso")
    private boolean success;

    public ApiResponse() {}

    public List<T> getItems() {
        return items;
    }

    public void setItems(List<T> items) {
        this.items = items;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }
}