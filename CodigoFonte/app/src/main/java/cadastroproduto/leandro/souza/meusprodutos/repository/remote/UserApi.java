package cadastroproduto.leandro.souza.meusprodutos.repository.remote;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;
import cadastroproduto.leandro.souza.meusprodutos.repository.model.Credentials;
import cadastroproduto.leandro.souza.meusprodutos.repository.model.User;

public interface UserApi {

    @POST("usuario/autenticar")
    Call<User> authenticate(@Body Credentials credentials);
}