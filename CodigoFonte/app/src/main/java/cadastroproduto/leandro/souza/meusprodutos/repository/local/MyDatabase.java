package cadastroproduto.leandro.souza.meusprodutos.repository.local;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;

import cadastroproduto.leandro.souza.meusprodutos.repository.model.Product;

@Database(entities = {Product.class}, version = 1)
public abstract class MyDatabase extends RoomDatabase {

    public abstract ProductDao getProductDao();
}