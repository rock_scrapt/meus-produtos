package cadastroproduto.leandro.souza.meusprodutos.repository.remote;

import retrofit2.Call;
import retrofit2.http.GET;
import cadastroproduto.leandro.souza.meusprodutos.repository.model.Product;

public interface ProductApi {

    @GET("produto/lista")
    Call<ApiResponse<Product>> getAll();
}