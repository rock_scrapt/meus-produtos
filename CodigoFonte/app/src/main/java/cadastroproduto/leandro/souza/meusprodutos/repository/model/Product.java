package cadastroproduto.leandro.souza.meusprodutos.repository.model;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

@Entity(tableName = "product")
public class Product implements Serializable {

    @PrimaryKey(autoGenerate = true)
    private int uid;

    @SerializedName("nome")
    @ColumnInfo(name = "name")
    private String name;

    @SerializedName("valor")
    @ColumnInfo(name = "value")
    private double value;

    @SerializedName("imagem")
    @ColumnInfo(name = "image")
    private String image;

    public Product() { }

    public int getUid() {
        return uid;
    }

    public void setUid(int uid) {
        this.uid = uid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getValue() {
        return value;
    }

    public void setValue(double value) {
        this.value = value;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}